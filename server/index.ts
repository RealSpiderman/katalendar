const { createMeetingResolver } = require("./resolvers/createMeetingResolver");
const {
  allMeetingsResolver,
  meetingResolver,
} = require("./resolvers/meetingResolver");
const { tagsResolver } = require("./resolvers/tagResolver");
const { defaultSchema } = require("./schemas");
const { deleteMeetingResolver } = require("./resolvers/deleteMeetingResolver");

const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const { buildSchema } = require("graphql");

const schema = buildSchema(defaultSchema);

const root = {
  meeting: meetingResolver,
  allMeetings: allMeetingsResolver,
  tag: tagsResolver,
  createMeeting: createMeetingResolver,
  deleteMeeting: deleteMeetingResolver,
};

const app = express();
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: false,
  })
);

app.use(
  "/graphiql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);
app.head;
app.listen(4000, () => console.log("Now browse to localhost:4000/graphql"));

module.exports = schema;
