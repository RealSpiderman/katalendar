export interface Tag {
  tagId: string;
  tagName: string;
}
export interface Meeting {
  id: string;
  title: string;
  description: string;
  endDate: string;
  startDate: string;
  price?: number;
  url?: string;
  tags: Tag[];
}

export interface InputMeeting {
  meeting: Meeting;
}
