export const firebaseTagMock = [
  {
    tagId: "123",
    tagName: "Graphql",
  },
  {
    tagId: "999",
    tagName: "Backend-services",
  },
  {
    tagId: "456",
    tagName: "Graphql",
  },
  {
    tagId: "789",
    tagName: "Backend-services",
  },
];

export const firebaseMock = [
  {
    id: "123",
    name: "GraphQL 101",
    description: "kaata on graphQL introductions",
    endDate: "01/02/2021",
    startDate: "03/02/2021",
    price: 20,
    url: "www.graphql.com",
    tags: [
      {
        tagId: "123",
        tagName: "Graphql",
      },
      {
        tagId: "999",
        tagName: "Backend-services",
      },
    ],
  },
  {
    id: "456",
    name: "React Advanced",
    description: "Implement advanced react functionality",
    endDate: "05/06/2021",
    startDate: "05/06/2021",
    price: 10,
    url: "www.react.com",
    tags: [
      {
        tagId: "123",
        tagName: "React",
      },
      {
        tagId: "999",
        tagName: "Intermediate",
      },
      {
        tagId: "999",
        tagName: "Frontend-tech",
      },
    ],
  },
];
