import firebase from "firebase/app";
import "firebase/firestore";
import { InputMeeting, Meeting } from "../types/Meeting";

let project: firebase.app.App;

export const getFirebaseProject = (): firebase.app.App => {
  if (project) return project;
  var firebaseConfig = {
    apiKey: "AIzaSyBcE3BV_tv5OZ7YcrBnc4EaJ4d0dKDW9I0",
    authDomain: "katalendar.firebaseapp.com",
    projectId: "katalendar",
    storageBucket: "katalendar.appspot.com",
    messagingSenderId: "639962741028",
    appId: "1:639962741028:web:614b0308b39f1dac669bb1",
    measurementId: "G-6ZNR77FT7L",
  };

  project = firebase.initializeApp(firebaseConfig);
  return project;
};

const FIREBASE_COLLECTION = "katas";

export const getMeetings = async (): Promise<
  firebase.firestore.DocumentData[]
> => {
  const project = getFirebaseProject();
  const db = project.firestore();
  try {
    const doc = await db.collection(FIREBASE_COLLECTION).get();
    const docsData = doc.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }));
    return docsData;
  } catch (error) {
    console.error(error);
    return [];
  }
};

export const createMeeting = async (
  inputMeeting: InputMeeting,
  id: string | undefined
) => {
  const project = getFirebaseProject();
  const db = project.firestore();
  try {
    await db.collection(FIREBASE_COLLECTION).doc(id).set(inputMeeting);
  } catch (error) {
    console.error(error);
  }
};

export const deleteMeeting = async (id: string): Promise<{ id: string }> => {
  const project = getFirebaseProject();
  const db = project.firestore();
  try {
    await db.collection(FIREBASE_COLLECTION).doc(id).delete();
    return { id };
  } catch (error) {
    console.error(error);
    return { id: "" };
  }
};
