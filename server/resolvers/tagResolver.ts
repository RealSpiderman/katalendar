import { firebaseTagMock } from "../mocks/firebaseMock";
import { Tag } from "../types/Meeting";

export const tagsResolver = (props: any) => {

  return firebaseTagMock.filter((tag: Tag) => {
    return props.ids.includes(tag.tagId);
  });
};
