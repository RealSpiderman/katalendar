import { createMeeting } from "../firebase/firebaseService";
import { InputMeeting } from "../types/Meeting";

export const createMeetingResolver = ({
  meeting,
  id,
}: {
  meeting: InputMeeting;
  id: string | undefined;
}) => {
  createMeeting(meeting, id);
};
