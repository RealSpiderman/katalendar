import {deleteMeeting} from "../firebase/firebaseService";

export const deleteMeetingResolver = async ({id}: { id: string }) => {
    return await deleteMeeting(id);
}