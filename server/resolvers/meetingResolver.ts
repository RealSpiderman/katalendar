import { getMeetings } from "../firebase/firebaseService";
import { firebaseMock } from "../mocks/firebaseMock";
import { Meeting } from "../types/Meeting";

export const meetingResolver = async (props: any) => {
  const doc = await getMeetings();
  return doc.find((meeting) => {
    return meeting.id === props.id;
  });
};

export const allMeetingsResolver = async (props: any) => {
  return await getMeetings();
};
