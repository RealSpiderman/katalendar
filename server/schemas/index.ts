export const defaultSchema = `
  type Tag {
    tagId: String!
    tagName: String
  }
  type Meeting {
    id: String!
    title: String
    description: String
    endDate: String
    startDate: String
    price: Float
    url: String
    tags: [Tag]
  }

  input InputTag {
    tagId: String!
    tagName: String
  }
  input InputMeeting {
    title: String
    description: String
    endDate: String
    startDate: String
    price: Float
    url: String
    tags: [InputTag]
  }

  type Query {
    meeting(id: String!): Meeting
    tag(ids: [String]): [Tag]
    allMeetings: [Meeting]
  }
  type Mutation {
    createMeeting(meeting: InputMeeting!, id: String) : ID
    deleteMeeting(id: String!): Meeting
  }
`;
