import { meetingApiUrl } from "../default";
import { adaptAllMeetingsResponse } from "../utils/adaptAllMeetingsResponse";

export const fetchMeetings = async () => {
  const graphqlQuery = `{
      allMeetings {
		id
		title
        startDate
        endDate
      }
	  }`;
  const response = await fetch(meetingApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ query: graphqlQuery }),
  });

  return await response.json().then((res) => {
    return adaptAllMeetingsResponse(res);
  });
};
