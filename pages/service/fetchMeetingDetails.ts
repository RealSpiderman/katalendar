import { meetingApiUrl } from "../default";
import { adaptMeetingResponse } from "../utils/adaptMeetingResponse";

export const fetchMeetingDetails = async (id) => {
  const graphqlQuery = `{
      meeting(id: "${id}") {
        id
        description
        title
        startDate
        endDate
        tags {
          tagName
        }
      }
	  }`;
  const response = await fetch(meetingApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ query: graphqlQuery }),
  });

  return await response.json().then((res) => {
    return adaptMeetingResponse(res.data.meeting);
  });
};
