import { meetingApiUrl } from "../default";
import { Tag } from "../types/Meeting";
import { InputMeeting } from "./createMeeting";

export interface UpdateMeeting extends InputMeeting {
  id: string;
}

const createTagString = (tags) => {
  const tagString = tags
    .map((tag) => `{tagId: "${tag.tagId}", tagName: "${tag.tagName}" }`)
    .join();
  return `[${tagString}]`;
};

export const updateMeeting = async (meeting: UpdateMeeting) => {
  const graphqlQuery = `
    mutation {
    createMeeting(meeting:{
        title:"${meeting.title}",
        description: "${meeting.description}",
        startDate:"${meeting.startDate}",
        endDate:"${meeting.endDate}",
        tags: ${createTagString(meeting.tags)}
      }, id: "${meeting.id}")
    }
    `;

  console.log(graphqlQuery);

  return await fetch(meetingApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ query: graphqlQuery }),
  });
};
