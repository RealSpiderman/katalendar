import { meetingApiUrl } from "../default";

export const deleteMeeting = async (meetingId: string) => {
  const graphqlQuery = `
    mutation {
      deleteMeeting(id:"${meetingId}") {
        id
      }
    }`;

  console.log(graphqlQuery);

  return await fetch(meetingApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ query: graphqlQuery }),
  });
};
