import { meetingApiUrl } from "../default";
import { Tag } from "../types/Meeting";

export interface InputMeeting {
  title: string;
  description: string;
  endDate: string;
  startDate: string;
  price?: number;
  url?: string;
  tags: Tag[];
}

const createTagString = (tags) => {
  const tagString = tags
    .map((tag) => `{tagId: "${tag.tagId}", tagName: "${tag.tagName}" }`)
    .join();
  return `[${tagString}]`;
};

export const createMeeting = async (meeting: InputMeeting) => {
  const graphqlQuery = `
    mutation {
    createMeeting(meeting:{
        title:"${meeting.title}",
        description: "${meeting.description}",
        startDate:"${meeting.startDate}",
        endDate:"${meeting.endDate}",
        tags: ${createTagString(meeting.tags)}
      })
    }
    `;

  console.log(graphqlQuery);

  return await fetch(meetingApiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ query: graphqlQuery }),
  });
};
