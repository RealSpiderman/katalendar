import React, {useEffect, useState} from "react";
import HyperModal from "react-hyper-modal";
import {fetchMeetingDetails} from "../../service/fetchMeetingDetails";
import {Meeting} from "../../types/Meeting";
import {deleteMeeting} from "../../service/deleteMeeting";
import Link from "next/link";
import {Button} from "@material-ui/core";

const renderTags = (tags) => {
    return (
        <>
            {tags.map((tag) => {
                return <p>{tag.tagName}</p>;
            })}
        </>
    );
};

const deleteEvent = (meetingId: string) => {
    deleteMeeting(meetingId);
};

export const EventModal = ({isOpen, closeModal, meetingId}) => {
    const [meetingDetails, setMeetingDetails] = useState<Meeting>(undefined);

    useEffect(() => {
        fetchMeetingDetails(meetingId).then((meetingDetailsPayload) => {
            console.log(meetingDetailsPayload);
            setMeetingDetails(meetingDetailsPayload);
        });
    }, []);
    return (
        <HyperModal
            isOpen={isOpen}
            requestClose={closeModal}
            classes={{
                wrapperClassName: "high-z-index",
            }}
        >
            {meetingDetails ? (
                <>
                    <h2 className="modal-title">{meetingDetails.title}</h2>
                    <div className="modal-content">
                        <label htmlFor="Description">Description</label>
                        <p id="Description">{meetingDetails.description}</p>
                        <label htmlFor="StartDate">start date</label>
                        <p id="StartDate">{meetingDetails.startDate.toString()}</p>
                        <label htmlFor="EndDate">end date</label>
                        <p id="EndDate">{meetingDetails.endDate.toString()}</p>
                        <label htmlFor="Tags">Tags</label>
                        <div id="Tags">{renderTags(meetingDetails.tags)}</div>

                        <button
                            onClick={() => {
                                deleteEvent(meetingDetails.id);
                                closeModal();
                            }}
                        >
                            Delete Event
                        </button>

                        <Link href={`/editMeeting?meetingId=${meetingId}`}>
                            <Button type="submit" variant="contained" color="primary">
                                Edit Meeting
                            </Button>
                        </Link>
                    </div>
                </>
            ) : (
                <>Loading...</>
            )}
        </HyperModal>
    );
};
