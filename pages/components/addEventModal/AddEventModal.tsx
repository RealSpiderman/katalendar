import React from "react";
import HyperModal from "react-hyper-modal";
import { AddMeetingForm } from "../addMeetingForm/AddMeetingForm";

export const AddEventModal = ({ isOpen, closeModal, slotInfo }) => {
  return (
    <HyperModal
      isOpen={isOpen}
      requestClose={closeModal}
      classes={{
        wrapperClassName: "high-z-index",
      }}
    >
      <div>
        <h2>Add new event</h2>
        <AddMeetingForm
          meetingDetails={{
            startDate: slotInfo?.start?.toISOString(),
            endDate: slotInfo?.end?.toISOString(),
          }}
          closeModal={closeModal}
        />
      </div>
    </HyperModal>
  );
};
