import React from "react";
import { MeetingContextProvider } from "../context/MeetingContext";
import { Calendar } from "./calendar/Calendar";

export const App = () => {
  return (
    <MeetingContextProvider>
      <Calendar />
    </MeetingContextProvider>
  );
};
