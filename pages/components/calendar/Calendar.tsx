import React, { useState } from "react";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import { EventModal } from "../eventModal/EventModal";
import { AddEventModal } from "../addEventModal/AddEventModal";
import { useMeetingContext } from "../../context/MeetingContext";

const localizer = momentLocalizer(moment);

export const Calendar = () => {
  const { meetings, updateMeetings } = useMeetingContext();
  const [isEventInfoModalOpen, setIsEventInfoModalOpen] = useState(false);
  const [isAddEventModalOpen, setIsAddEventModalOpen] = useState(false);
  const [selectedEvent, setSelectedEvent] = useState<any>({});
  const [selectedSlotInfo, setselectedSlotInfo] = useState<any>();

  return (
    <div>
      {isEventInfoModalOpen && (
        <EventModal
          meetingId={selectedEvent.id}
          isOpen={isEventInfoModalOpen}
          closeModal={() => {
            updateMeetings();
            setIsEventInfoModalOpen(false);
          }}
        />
      )}
      {isAddEventModalOpen && (
        <AddEventModal
          isOpen={isAddEventModalOpen}
          closeModal={() => {
            updateMeetings();
            setIsAddEventModalOpen(false);
          }}
          slotInfo={selectedSlotInfo}
        />
      )}
      <BigCalendar
        localizer={localizer}
        events={meetings}
        // startAccessor="start"
        // endAccessor="end"
        style={{ height: 500 }}
        onSelectEvent={(event) => {
          console.log("Event", event);
          setSelectedEvent(event);
          setIsEventInfoModalOpen(true);
        }}
        selectable={true}
        onSelectSlot={(slotInfo) => {
          console.log("SeLECT SLOT");
          setselectedSlotInfo(slotInfo);
          setIsAddEventModalOpen(true);
        }}
      />
    </div>
  );
};
