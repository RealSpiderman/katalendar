import { createMeeting } from "../../service/createMeeting";
import { Button, Grid, TextField } from "@material-ui/core";
import moment from "moment";
import ChipInput from "material-ui-chip-input";
import React, { useState } from "react";
import { Meeting, Tag } from "../../types/Meeting";
import { v4 as uuidv4 } from "uuid";
import { updateMeeting } from "../../service/updateMeeting";
interface AddMeetingFormProps {
  meetingDetails: Partial<Meeting>;
  closeModal: Function;
}

export const AddMeetingForm: React.FC<AddMeetingFormProps> = ({
  meetingDetails,
  closeModal,
}) => {
  const startDateString = moment(meetingDetails?.startDate).format(
    "yyyy-MM-DDThh:mm"
  );
  const endDateString = moment(meetingDetails?.endDate).format(
    "yyyy-MM-DDThh:mm"
  );
  const isEdit = !!meetingDetails.id;
  const [title, setTitle] = useState(meetingDetails.title);
  const [description, setDescription] = useState(meetingDetails.description);
  const [startDate, setStartDate] = useState(startDateString);
  const [endDate, setEndDate] = useState(endDateString);
  const [tags, setTags] = useState<Tag[]>(meetingDetails.tags ?? []);

  return (
    <form
      onSubmit={(e) => {
        console.log({
          title,
          description,
          startDate,
          endDate,
          tags,
        });
        e.preventDefault();
        if (isEdit) {
          updateMeeting({
            title,
            description,
            startDate,
            endDate,
            tags,
            id: meetingDetails.id,
          });
        } else {
          createMeeting({ title, description, startDate, endDate, tags });
        }
        closeModal();
      }}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        spacing={2}
      >
        <Grid item xs={12}>
          <TextField
            type="input"
            variant="outlined"
            label="Meeting title"
            id="title-input"
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type="input"
            variant="outlined"
            label="Meeting description"
            id="description-input"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type="datetime-local"
            id="start-date-input"
            variant="outlined"
            label="Meeting start date"
            value={startDate}
            onChange={(e) => {
              const formattedDate = moment(
                new Date(e.target.value).toISOString()
              ).format("yyyy-MM-DDThh:mm");
              setStartDate(formattedDate);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type="datetime-local"
            variant="outlined"
            label="Meeting end date"
            id="end-date-input"
            value={endDate}
            onChange={(e) => {
              const formattedDate = moment(
                new Date(e.target.value).toISOString()
              ).format("yyyy-MM-DDThh:mm");
              setEndDate(formattedDate);
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <ChipInput
            id="tags-input"
            label="tags (separated by ,)"
            value={tags.map((tag) => tag.tagName)}
            onChange={(chips) =>
              setTags(
                chips
                  ? chips.map((tagName) => ({
                      tagName: tagName,
                      tagId: uuidv4(),
                    }))
                  : []
              )
            }
          />
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary">
            {isEdit ? "Edit the event" : "Add new event"}
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};
