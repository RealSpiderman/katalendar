import Head from "next/head";
import React from "react";
import styles from "../styles/Home.module.css";
import { App } from "./components/App";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Katalendar</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <App />
    </div>
  );
}
