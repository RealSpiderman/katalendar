import { createMeetingResolver } from "../../../server/resolvers/createMeetingResolver";
import {
  allMeetingsResolver,
  meetingResolver,
} from "../../../server/resolvers/meetingResolver";
import { tagsResolver } from "../../../server/resolvers/tagResolver";
import { defaultSchema } from "../../../server/schemas";
import { deleteMeetingResolver } from "../../../server/resolvers/deleteMeetingResolver";
var { graphql, buildSchema } = require("graphql");

var schema = buildSchema(defaultSchema);

var root = {
  meeting: meetingResolver,
  allMeetings: allMeetingsResolver,
  tag: tagsResolver,
  createMeeting: createMeetingResolver,
  deleteMeeting: deleteMeetingResolver,
};

export default async (req, res) => {
  const response = await graphql(schema, req.body.query, root);
  res.status(200).json(response);
};
