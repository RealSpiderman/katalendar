import constate from "constate";
import { useEffect, useState } from "react";
import { fetchMeetings } from "../service/fetchMeetings";
import { Meeting } from "../types/Meeting";

interface MeetingContextModel {
  meetings: Meeting[];
  updateMeetings: () => void;
}

const MeetingContextHook = (): MeetingContextModel => {
  const [meetings, setMeetings] = useState([] as any);

  const updateMeetings = () => {
    fetchMeetings().then((meetings) => {
      const eventsList = meetings.map((meeting) => ({
        id: meeting.id,
        title: meeting.title,
        allDay: true,
        start: meeting.startDate,
        end: meeting.endDate,
      }));
      setMeetings(eventsList);
    });
  };
  useEffect(() => {
    updateMeetings();
  }, []);

  return {
    meetings,
    updateMeetings,
  };
};

const [MeetingContextProvider, useMeetingContext] = constate<
  {},
  MeetingContextModel,
  []
>(MeetingContextHook);

export { MeetingContextProvider, useMeetingContext };
