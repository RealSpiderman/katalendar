import Head from "next/head";
import React, { useEffect, useState } from "react";
import styles from "../../styles/Home.module.css";
import { AddMeetingForm } from "../components/addMeetingForm/AddMeetingForm";
import { useRouter } from "next/router";
import { fetchMeetingDetails } from "../service/fetchMeetingDetails";
import { Meeting } from "../types/Meeting";

export default function Home() {
  const Router = useRouter();
  const [meetingDetails, setMeetingDetails] = useState<Meeting>();

  useEffect(() => {
    const meetingId = Router.query.meetingId;
    if (meetingId) {
      fetchMeetingDetails(meetingId).then((details) => {
        console.log("meetingDetails", details);
        setMeetingDetails(details);
      });
    }
  }, [Router.query]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Katalendar</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <h1>Edit Meeting</h1>
      <div style={{ width: "100%" }}>
        {meetingDetails ? (
          <AddMeetingForm
            meetingDetails={meetingDetails}
            closeModal={() => {
              Router.push("/");
            }}
          />
        ) : (
          <>Loading...</>
        )}
      </div>
    </div>
  );
}
