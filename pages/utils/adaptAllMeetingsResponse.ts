export const adaptAllMeetingsResponse = (meetingResponse) => {
  return meetingResponse.data.allMeetings.map((meeting) => ({
    ...meeting,
    startDate: new Date(meeting.startDate),
    endDate: new Date(meeting.endDate),
  }));
};
