export const adaptMeetingResponse = (meetingResponse) => {
  return {
    ...meetingResponse,
    startDate: new Date(meetingResponse.startDate),
    endDate: new Date(meetingResponse.endDate),
  };
};
