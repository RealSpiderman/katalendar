export interface Tag {
  tagId: string;
  tagName: string;
}
export interface Meeting {
  id: string;
  title: string;
  description: string;
  endDate: Date;
  startDate: Date;
  price?: number;
  url?: string;
  tags: Tag[];
}
